﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AutoHidingScrollbar : MonoBehaviour
{
    [SerializeField] private ScrollRect scroll = null;
    [SerializeField] private RectTransform viewport = null;
    [SerializeField] private RectTransform titleRow = null;
    [SerializeField] private RectTransform content = null;
    [SerializeField] private Scrollbar scrollbar = null;

    private float originalViewportWidth;
    private float originalTitleRowWidth;
    
    private void Start()
    {
        originalViewportWidth = viewport.sizeDelta.x;
        if (titleRow == null) return;
        originalTitleRowWidth = titleRow.sizeDelta.x;
    }

    private void OnEnable()
    {
        SnapToTop();
    }

    private void Update()
    {
        if (content.rect.height > viewport.rect.height && !scroll.enabled)
        {
            SnapToTop();
            viewport.sizeDelta = new Vector2(originalViewportWidth, viewport.sizeDelta.y);
            if (titleRow != null)
                titleRow.sizeDelta = new Vector2(originalTitleRowWidth, titleRow.sizeDelta.y);
            scroll.enabled = true;
            scrollbar.gameObject.SetActive(true);
        }
        else if (content.rect.height <= viewport.rect.height && scroll.enabled)
        {
            SnapToTop();
            scroll.enabled = false;
            viewport.sizeDelta = new Vector2(0, viewport.sizeDelta.y);
            if (titleRow != null)
                titleRow.sizeDelta = new Vector2(0, titleRow.sizeDelta.y);
            scrollbar.gameObject.SetActive(false);
        }
    }

    private void SnapToTop()
    {
        //Debug.Log("Snapping to top");
        content.anchoredPosition = new Vector2(content.anchoredPosition.x, 0);
    }


}
