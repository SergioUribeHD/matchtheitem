﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FactorStretcher : MonoBehaviour
{
    [SerializeField] private bool stretchWidth = false;
    [SerializeField] private bool stretchHeight = true;

    private RectTransform rectTransform;
    private Vector2 initialRectDimensions;
    private Vector2 initialAnchoredPos;

    private void Start()
    {
        rectTransform = (RectTransform)transform;
        initialRectDimensions = new Vector2(rectTransform.rect.width, rectTransform.rect.height);
        initialAnchoredPos = rectTransform.anchoredPosition;
    }

    private void Update()
    {
        float factor = 1 / transform.localScale.x;
        if (stretchHeight)
        {
            rectTransform.sizeDelta = new Vector2(initialRectDimensions.x, initialRectDimensions.y * factor);
            rectTransform.anchoredPosition = new Vector2(initialAnchoredPos.x, initialAnchoredPos.y * transform.localScale.x);
        }
        else if (stretchWidth)
        {
            rectTransform.sizeDelta = new Vector2(initialRectDimensions.x * factor, initialRectDimensions.y);
            rectTransform.anchoredPosition = new Vector2(initialAnchoredPos.x, initialAnchoredPos.y * transform.localScale.x);
        }

    }
}
