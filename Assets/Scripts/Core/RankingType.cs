﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum RankingType
{
    NotDefined,
    User,
    Category,
    EnrollmentGroup,
    EnrollmentGroupsTotal
}

public enum SubRankingType
{
    NotDefined,
    Points,
    Accumulated,
    MaxStage,
    Attempts
}