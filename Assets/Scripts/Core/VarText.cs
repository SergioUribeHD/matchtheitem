using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class VarText
{
    [SerializeField] private Text text = null;
    [SerializeField] private string[] varCodes = new string[0];

    private string original;

    public void Update(object[] replacingValues)
    {
        if (replacingValues.Length != varCodes.Length)
        {
            Debug.LogError($"Var codes length of {varCodes.Length} does not match replacing values length of {replacingValues.Length}.");
            return;
        }
        SetOriginalText();
        text.text = original;
        for (int i = 0; i < replacingValues.Length; i++)
            text.text = text.text.Replace(varCodes[i], replacingValues[i].ToString());
    }

    public void Update(object replacingValue) => Update(new object[1] { replacingValue });

    private void SetOriginalText()
    {
        if (!string.IsNullOrEmpty(original)) return;
        original = text.text;
    }
}
