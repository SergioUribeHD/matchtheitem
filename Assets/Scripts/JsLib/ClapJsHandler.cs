using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class ClapJsHandler : MonoBehaviour
{
#if UNITY_WEBGL && !UNITY_EDITOR
    [DllImport("__Internal")]
    public static extern void AlertLog(bool succeeded, string logStr);

    [DllImport("__Internal")]
    public static extern void ReportCompleted(string attemptJson, string url);
#endif

}

[System.Serializable]
public class AttemptData
{
    public long instanceid;
    public long lessonid;
    public int points;


}
