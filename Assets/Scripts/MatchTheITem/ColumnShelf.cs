using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColumnShelf : MonoBehaviour
{
    [field: SerializeField] public Drawer[] Drawers { get; private set; } = new Drawer[0];

    private DrawerCheck drawerCheck;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.TryGetComponent<DrawerCheck>(out var drawerCheck)) return;
        this.drawerCheck = drawerCheck;
        drawerCheck.InColumn = true;
        drawerCheck.ColumnPosX = transform.position.x;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (drawerCheck == null || drawerCheck.InColumn) return;
        drawerCheck.InColumn = true;
        drawerCheck.ColumnPosX = transform.position.x;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (!collision.TryGetComponent<DrawerCheck>(out var drawerCheck)) return;
        this.drawerCheck = null;
        drawerCheck.InColumn = false;
    }
}
