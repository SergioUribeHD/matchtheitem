using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drawer : MonoBehaviour
{
    [SerializeField] private SpriteRenderer closedDrawer = null;
    [SerializeField] private GameObject openedDrawer = null;
    [SerializeField] private Color blockedColor = new Color(0.3f, 0.3f, 0.3f, 1f);

    public static event System.Action<int> OnOpen = null;

    public bool IsOpen { get; private set; }

    public bool Blocked { get; private set; }

    private static int drawersOpenedCount;


    private void Start()
    {
        drawersOpenedCount = 0;
    }

    public void Block()
    {
        Blocked = true;
        closedDrawer.color = blockedColor;
    }


    //Serialized for debugging purposes
    //[field: SerializeField] 
    public Sprite ToolSpr { get; set; }

    public void SetDoorOpen(bool open)
    {
        IsOpen = open;
        if (open && !openedDrawer.activeSelf)
        {
            drawersOpenedCount++;
            OnOpen?.Invoke(drawersOpenedCount);
        }
        openedDrawer.SetActive(IsOpen && !Blocked);
        closedDrawer.gameObject.SetActive(!IsOpen || Blocked);
    }

    public void ToggleDoor() => SetDoorOpen(!IsOpen);

}
