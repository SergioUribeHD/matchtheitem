using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawerCheck : MonoBehaviour
{
    [SerializeField] private SelectedToolDisplay contratistTool = null;
    [SerializeField] private GameObject vignette = null;
    [SerializeField] private SpriteRenderer toolSprRenderer = null;
    [SerializeField] private Collider2D matchingToolsCollider = null;

    //Serialized for debugging purposes
    //[field: SerializeField]
    public Drawer CurrentDrawer { get; set; }
    public float ColumnPosX { get; set; }
    public bool InColumn { get; set; }

    private void Start()
    {
        matchingToolsCollider.enabled = false;
    }

    public void GrabTool()
    {
        if (CurrentDrawer == null || CurrentDrawer.Blocked || matchingToolsCollider.enabled) return;
        CurrentDrawer.SetDoorOpen(true);
        vignette.SetActive(true);
        toolSprRenderer.sprite = CurrentDrawer.ToolSpr;
        CheckToolsMatch();
    }

    private void CheckToolsMatch()
    {
        if (matchingToolsCollider.enabled = contratistTool.ToolSprRenderer.sprite.Equals(toolSprRenderer.sprite))
            CurrentDrawer.Block();
    }

    public void ResetCheck()
    {
        matchingToolsCollider.enabled = false;
        vignette.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (matchingToolsCollider.enabled || !collision.TryGetComponent<Drawer>(out var drawer)) return;
        CurrentDrawer = drawer;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (!collision.TryGetComponent<Drawer>(out var drawer)) return;
        drawer.SetDoorOpen(false);
        CurrentDrawer = null;
        if (matchingToolsCollider.enabled) return;
        vignette.SetActive(false);
    }

}
