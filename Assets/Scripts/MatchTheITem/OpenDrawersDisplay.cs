using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDrawersDisplay : MonoBehaviour
{
    [SerializeField] private VarText openDrawersText = null;

    private void Awake()
    {
        Drawer.OnOpen += HandleDrawerOpen;
    }

    private void Start()
    {
        HandleDrawerOpen(0);
    }

    private void OnDestroy()
    {
        Drawer.OnOpen -= HandleDrawerOpen;
    }

    private void HandleDrawerOpen(int count)
    {
        openDrawersText.Update(count);
    }
}
