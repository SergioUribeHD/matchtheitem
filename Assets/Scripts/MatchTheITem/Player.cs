using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private DrawerCheck drawerCheck = null;
    [SerializeField] private float moveSpeed = 8f;
    [SerializeField] private string groundTag = "Ground";

    [SerializeField] private Animator anim = null;
    [SerializeField] private SpriteRenderer sprRenderer = null;
    
    [field: SerializeField]
    public bool InGround { get; private set; } = true;
    public bool BlockInputs { get; set; }

    private Vector2 movement;
    

    private void Update()
    {
        HandleInputs();
        UpdateAnimator();
        if (movement.x != 0)
            sprRenderer.flipX = movement.x < 0;
    }

    private void HandleInputs()
    {
        if (BlockInputs)
        {
            movement = Vector3.zero;
            return;
        }
        movement.x = InGround ? Input.GetAxisRaw("Horizontal") : 0;
        movement.y = drawerCheck.InColumn ? Input.GetAxisRaw("Vertical") : 0;
        if (!InGround)
            transform.position = new Vector3(drawerCheck.ColumnPosX, transform.position.y, transform.position.z);
        if (Input.GetButtonDown("Jump"))
            drawerCheck.GrabTool();
    }

    private void UpdateAnimator()
    {
        anim.SetBool("Moving", Mathf.Abs(movement.x) > 0);
        anim.SetBool("Climbing", Mathf.Abs(movement.y) > 0);
        anim.SetBool("InGround", InGround);
    }

    private void FixedUpdate()
    {
        transform.position += (Vector3)movement * moveSpeed * Time.fixedDeltaTime;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!collision.gameObject.tag.Equals(groundTag)) return;
        InGround = true;
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (!collision.gameObject.tag.Equals(groundTag)) return;
        InGround = false;
    }



}
