using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestionProgressDisplay : MonoBehaviour
{
    [SerializeField] private VarText progress = null;

    private void Awake()
    {
        StageHandler.OnProgressUpdated += HandleProgressUpdated;
    }

    private void OnDestroy()
    {
        StageHandler.OnProgressUpdated -= HandleProgressUpdated;
    }

    private void HandleProgressUpdated(int current, int total)
    {
        progress.Update(new object[] { current, total });
    }
}