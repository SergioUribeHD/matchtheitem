using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreStatsDisplay : MonoBehaviour
{
    [SerializeField] private VarText scoreStats = null;

    private void Awake()
    {
        ScoreHandler.OnBonusAdded += HandleBonusUpdated;
    }

    private void OnDestroy()
    {
        ScoreHandler.OnBonusAdded -= HandleBonusUpdated;
    }

    private void HandleBonusUpdated(int gameScore, float bonusFactor, int totalScore)
    {
        scoreStats.Update(new object[3] { gameScore, $"{bonusFactor * 100:0.#}", totalScore });
    }
}
