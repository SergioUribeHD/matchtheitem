using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectedToolDisplay : MonoBehaviour
{
    [SerializeField] private ShelfHandler shelf = null;
    [SerializeField] private GameObject vignette = null;
    [field: SerializeField] public SpriteRenderer ToolSprRenderer { get; private set; }

    private void Awake()
    {
        shelf.OnSelectedToolUpdated += HandleSelectedToolUpdated;
    }

    private void OnDestroy()
    {
        shelf.OnSelectedToolUpdated -= HandleSelectedToolUpdated;
    }

    private void HandleSelectedToolUpdated(Sprite selectedTool)
    {
        ToolSprRenderer.sprite = selectedTool;
        vignette.SetActive(true);
    }
}
