using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ShelfHandler : MonoBehaviour
{
    [field: SerializeField] public ColumnShelf[] Columns { get; private set; } = new ColumnShelf[0];
    [SerializeField] private Sprite[] toolSprites = null;
    [field: SerializeField] public UnityEvent OnAllToolsFound { get; private set; }

    private List<Sprite> inGameTools = new List<Sprite>();

    public event System.Action<Sprite> OnSelectedToolUpdated = null;


    private void Start()
    {
        var tools = new List<Sprite>(toolSprites);
        foreach (var column in Columns)
        {
            foreach (var drawer in column.Drawers)
            {
                if (tools.Count == 0)
                {
                    Debug.LogError("There are not enough tools for this shelf.");
                    return;
                }
                int randomSprIndex = Random.Range(0, tools.Count);
                drawer.ToolSpr = tools[randomSprIndex];
                tools.RemoveAt(randomSprIndex);
                inGameTools.Add(drawer.ToolSpr);
            }
        }
        TryUpdateSelectedTool();
    }
    public void TryUpdateSelectedTool()
    {
        if (inGameTools.Count == 0)
            OnAllToolsFound?.Invoke();
        else
            UpdateSelectedTool();
    }

    private void UpdateSelectedTool()
    {
        var selectedTool = inGameTools[Random.Range(0, inGameTools.Count)];
        inGameTools.Remove(selectedTool);
        OnSelectedToolUpdated?.Invoke(selectedTool);
    }

}
