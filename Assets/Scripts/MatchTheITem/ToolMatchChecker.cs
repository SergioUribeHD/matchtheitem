using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ToolMatchChecker : MonoBehaviour
{
    [field: SerializeField] public UnityEvent OnToolsMatch { get; private set; }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.TryGetComponent<DrawerCheck>(out var checker)) return;
        OnToolsMatch?.Invoke();
        checker.ResetCheck();
    }

}
