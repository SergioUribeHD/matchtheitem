using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

public class DeviceChecker : MonoBehaviour
{
    [SerializeField] private Text checkText = null;


#if !UNITY_EDITOR && UNITY_WEBGL
    [DllImport("__Internal")]
    public static extern bool IsMobile();
#endif

    private void Start()
    {
#if !UNITY_EDITOR && UNITY_WEBGL
        Device.IsMobileOrTablet = IsMobile();
        checkText.text = Device.IsMobileOrTablet ? "Mobile" : "PC";
#endif
    }
}
