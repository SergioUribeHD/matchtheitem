using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MenuHandler : MonoBehaviour
{
    [SerializeField] private WebDataRetriever dataRetriever = null;
    [SerializeField] private GameObject loadingPanel = null;
    [SerializeField] private UnityEvent onGameReady = null;

    public void PlayGame()
    {
        dataRetriever.RunTest();
        StartCoroutine(WaitForDataReady());
    }

    private IEnumerator WaitForDataReady()
    {
        while (!dataRetriever.DataReady || !dataRetriever.UrlReady)
        {
            if (!loadingPanel.activeSelf) loadingPanel.SetActive(true);         
            yield return null;
        }
        loadingPanel.SetActive(false);
        onGameReady?.Invoke();
    }

}
