using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

public class WebDataRetriever : MonoBehaviour
{
    [SerializeField] private string jsonTest = null;

    public bool UrlReady { get; private set; }

    public bool DataReady { get; private set; }

    public void RunTest()
    {
#if UNITY_EDITOR
        SetUrl("Yay");
        SetData(jsonTest);
#endif
    }

    public void SetUrl(string url)
    {
        WebsiteData.Url = url;
        UrlReady = true;
    }

    public void SetData(string json)
    {
        var dataNode = JSON.Parse(json);
        SetScoreList(dataNode[0]);
        SetQuestions(dataNode[1]);
        SetUserValues(dataNode[2]);
        DataReady = true;

    }

    private void SetScoreList(JSONNode data)
    {
        foreach (var userDataNode in data)
        {
            User.UsersScore.Add(new ScoreData(userDataNode));
        }
    }

    private void SetQuestions(JSONNode data)
    {
        foreach (var questionNode in data)
        {
            User.Questions.Add(JsonUtility.FromJson<QuestionData>(questionNode.Value.ToString()));
        }
    }

    private void SetUserValues(JSONNode data)
    {
        var dat = new UserData(data);
        User.LogIn(dat);
    }
}
