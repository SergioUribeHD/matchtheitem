using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cronometer : MonoBehaviour
{
    public event System.Action<float> OnTimeUpdated = null;
    public bool Stopped { get; set; }

    public float CurrentTime { get; private set; }


    private void Update()
    {
        if (Stopped) return;
        CurrentTime += Time.deltaTime;
        OnTimeUpdated?.Invoke(CurrentTime);
    }
}
