﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LivesDisplay : MonoBehaviour
{
    [SerializeField] LivesController livesController = null;
    [SerializeField] private Image[] liveImgs = new Image[0];
    [SerializeField] private Color liveLostColor = Color.black;

    private void Awake()
    {
        livesController.OnLivesUpdated.AddListener(HandleLivesUpdated);
    }
    private void HandleLivesUpdated()
    {
        int currentLivesIndex = livesController.InitialLives - 1 - livesController.LivesAmount;
        liveImgs[currentLivesIndex].color = liveLostColor;
    }
}
