﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionContainer : MonoBehaviour
{
    [SerializeField] private Text questionText = null;
    [SerializeField] private List<OptionContainer> optionContainers = new List<OptionContainer>();
    [SerializeField] private TextSizer textSizer = null;
    [SerializeField] private GameObject blockPanel = null;

    public int OptionContainersAmount => optionContainers.FindAll(o => o.gameObject.activeSelf).Count;
    
    private void Awake()
    {
        OptionContainer.OnSingleChoiceSubmission += HandleOptionSelected;
        OptionContainer.OnMultipleChoiceSubmission += HandleMultipleChoiceSubmission;
    }

    private void OnDestroy()
    {
        OptionContainer.OnSingleChoiceSubmission -= HandleOptionSelected;
        OptionContainer.OnMultipleChoiceSubmission -= HandleMultipleChoiceSubmission;
    }

    public void SetQuestion(QuestionData question)
    {
        OptionContainer.CorrectCount = 0;
        blockPanel.SetActive(false);
        textSizer.ResetBestFit();
        questionText.text = question.text;
        SetAnswers(question.answer, question.multiresponse);
        textSizer.AdjustTextSizes();
    }

    public void HandleOptionSelected(OptionContainer option)
    {
        blockPanel.SetActive(true);
        if (option.IsCorrect) return;
        var correctOption = optionContainers.Find(o => o.IsCorrect);
        correctOption.Review();
    }

    public void HandleMultipleChoiceSubmission(List<OptionContainer> selectedOptions)
    {
        blockPanel.SetActive(true);
    }
    
    public void ReviewAllOptions()
    {
        blockPanel.SetActive(true);
        foreach (var option in optionContainers)
            option.Review();
    }

    public OptionContainer GetRandomWrongOption() => optionContainers.FindAll(o => o.gameObject.activeSelf && !o.IsCorrect).GetRandomItem();

    private void SetAnswers(Option[] options, bool multipleChoice)
    {
        if (options.Length > optionContainers.Count)
        {
            Debug.LogError($"There are {options.Length} options, and only {optionContainers.Count} option containers. Make sure to add more on {name}.");
            return;
        }

        var sortedOptions = new List<Option>(options);

        for (int i = 0; i < options.Length; i++)
        {
            var randomOption = sortedOptions[Random.Range(0, sortedOptions.Count)];
            sortedOptions.Remove(randomOption);
            optionContainers[i].SetAnswer(randomOption, multipleChoice, i);
        }

        for (int i = options.Length; i < optionContainers.Count; i++)
        {
            if (optionContainers[i].gameObject.activeSelf)
                optionContainers[i].gameObject.SetActive(false);
        }

    }


}

[System.Serializable]
public class QuestionData
{
    public long id;
    public string text;
    public bool multiresponse;
    public Option[] answer;

    public Option[] GetCorrectOptions() => new List<Option>(answer).FindAll(o => o.rightAnswer).ToArray();
}


[System.Serializable]
public class Option
{
    public string text;
    public Feedback feedback;
    public bool rightAnswer;
}


[System.Serializable]
public class Feedback
{
    public string text;
}