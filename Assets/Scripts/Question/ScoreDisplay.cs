﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreDisplay : MonoBehaviour
{
    [SerializeField] private VarText scoreText = null;

    private void Awake()
    {
        ScoreHandler.OnScoreUpdated += HandleScoreUpdated;
    }

    private void OnDestroy()
    {
        ScoreHandler.OnScoreUpdated -= HandleScoreUpdated;
    }

    private void HandleScoreUpdated(int newScore)
    {
        scoreText.Update(newScore.ToString());
    }
}
