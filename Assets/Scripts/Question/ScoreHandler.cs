﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreHandler : MonoBehaviour
{
    [SerializeField] private int scorePerQuestion = 50;
    [SerializeField] private Cronometer cronometer = null;
    [SerializeField] private float maxTimeForBonus = 0f;

    public static event Action<int> OnScoreUpdated = null;
    public static event Action<int, float, int> OnBonusAdded = null;
    public static event Action OnStageAttemptAdded = null;

    private float MinTimeForWholeBonus => maxTimeForBonus / 2;

    private int timeBonus;

    public int Score
    {
        get => score;
        private set
        {
            score = value;
            OnScoreUpdated?.Invoke(value);
        }
    }

    private int score;

    private void Awake()
    {
        StageHandler.OnQuestionUpdated += HandleQuestionUpdated;
    }

    private void Start()
    {
        Score = 0;
    }

    private void OnDestroy()
    {
        StageHandler.OnQuestionUpdated -= HandleQuestionUpdated;
    }

    public void AddScore()
    {
        Score += scorePerQuestion + timeBonus;
    }

    private void HandleQuestionUpdated(QuestionData question)
    {
        timeBonus = 0;
    }

    private void AddBonus(bool win)
    {
        cronometer.Stopped = true;
        int gameScore = Score;
        float bonusFactor = GetBonusFactor(win);
        Score += Mathf.RoundToInt(bonusFactor * gameScore);
        OnBonusAdded?.Invoke(gameScore, bonusFactor, Score);
        //print($"Having a remaining time of {remainingTime}, the bonus is {timeBonus}");
    }

    private float GetBonusFactor(bool win)
    {
        if (!win || cronometer.CurrentTime >= maxTimeForBonus) return 0;
        float diff = cronometer.CurrentTime - MinTimeForWholeBonus;
        return 1 - (diff > 0 ? diff / MinTimeForWholeBonus : 0);
    }

    public void HandleStageFinished(bool win)
    {
        if (cronometer != null)
            AddBonus(win);
        var attemptData = new AttemptData { points = Score };
#if UNITY_WEBGL && !UNITY_EDITOR
        ClapJsHandler.ReportCompleted(JsonUtility.ToJson(attemptData), WebsiteData.Url);
#endif
    }

}
