﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SummaryQuestionLine : MonoBehaviour
{
    [SerializeField] private Text questionTitleText = null;
    [SerializeField] private Text correctOptionText = null;
    [SerializeField] private Text feedbackText = null;
    [SerializeField] private Image reviewImg = null;
    [SerializeField] private Sprite correctSpr = null;
    [SerializeField] private Sprite wrongSpr = null;
    [SerializeField] private Color rightOption = new Color(0, 1, 0.6476f, 1);
    [SerializeField] private Color wrongOption = Color.red;


    public void Set(QuestionSummary questionSummary)
    {
        reviewImg.sprite = questionSummary.ChoseCorrectOption ? correctSpr : wrongSpr;
        questionTitleText.text = questionSummary.QuestionTitle;
        SetCorrectOptionText(questionSummary);
        feedbackText.text = questionSummary.Feedback;
    }

    private void SetCorrectOptionText(QuestionSummary questionSummary)
    {
        if (questionSummary.CorrectOption != null)
            correctOptionText.text = questionSummary.CorrectOption.text;
        else
        {
            var selectedOptions = questionSummary.SelectedOptions;
            correctOptionText.text = GetAnswerLine(selectedOptions[0]);
            for (int i = 1; i < selectedOptions.Length; i++)
                correctOptionText.text += $"\n{GetAnswerLine(selectedOptions[i])}";
        }    
    }

    private string GetAnswerLine(Option option)
    {
        Color color = option.rightAnswer ? rightOption : wrongOption;
        return color.ToRichText($"- {option.text}");
    }

}
