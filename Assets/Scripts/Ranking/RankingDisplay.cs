using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RankingDisplay : MonoBehaviour
{
    [SerializeField] private List<RankingRow> rankingRows = new List<RankingRow>();
    [SerializeField] private RectTransform content = null;

    private void OnEnable()
    {
        CheckRowsNeeded();
        UpdateRows();
    }

    private void CheckRowsNeeded()
    {
        int diff = User.UsersScore.Count - rankingRows.Count;
        if (diff > 0)
            AddRows(diff);
        else if (diff < 0)
            DisableRemainingRows(diff);
    }

    private void AddRows(int diff)
    {
        for (int i = 0; i < diff; i++)
            rankingRows.Add(Instantiate(rankingRows.GetLastItem(), content));
    }

    private void DisableRemainingRows(int diff)
    {
        for (int i = rankingRows.Count + diff; i < rankingRows.Count; i++)
            rankingRows[i].gameObject.SetActive(false);
    }

    private void UpdateRows()
    {
        for (int i = 0; i < User.UsersScore.Count; i++)
            rankingRows[i].Set(User.UsersScore[i], i + 1);
    }
}
