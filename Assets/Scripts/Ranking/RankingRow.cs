using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RankingRow : MonoBehaviour
{
    [SerializeField] private Text positionText = null;
    [SerializeField] private Text usernameText = null;
    [SerializeField] private VarText scoreText = null;

    public void Set(ScoreData data, int position)
    {
        gameObject.SetActive(true);
        if (positionText != null)
            positionText.text = position.ToString();
        usernameText.text = data.Name;
        scoreText.Update(data.Points.ToString());
    }

    private void OnDisable()
    {
        usernameText.text = string.Empty;
        scoreText.Update(string.Empty);
    }

}
