﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

public class RestApiRequest : Singleton<RestApiRequest>
{
    [SerializeField] private GameObject loadingPanel = null;
    [SerializeField] private bool debugAuthResponse = false;

    public const string Texture = "TEX";

    private int numOperations;

    public void GetJson(RestApiObject restObject)
    {
        GetJson(restObject.uri, restObject.token, restObject.onSuccess, restObject.onError);
    }

    public void GetJson(string uri, string token, Action<DownloadHandler> onSuccess, Action<long> onError)
    {
        StartCoroutine(SendRequest(uri, null, UnityWebRequest.kHttpVerbGET, token, onSuccess, onError));
    }

    public void PostJson(RestApiObject restObject)
    {
        PostJson(restObject.uri, restObject.bodyJson, restObject.token, restObject.onSuccess, restObject.onError);
    }

    public void PostJson(string uri, object bodyJson, Action<DownloadHandler> onSuccess, Action<long> onError)
    {
        PostJson(uri, bodyJson, null, onSuccess, onError);
    }

    public void PostJson(string uri, object body, string token, Action<DownloadHandler> onSuccess, Action<long> onError)
    {
        StartCoroutine(SendRequest(uri, body, UnityWebRequest.kHttpVerbPOST, token, onSuccess, onError));
    }

    public void TryGetTexture(string url, Action<Texture2D> onSuccess, Action<long> onError)
    {
        StartCoroutine(SendRequest(url, null, Texture, d => onSuccess(((DownloadHandlerTexture)d).texture), onError));
    }

    private IEnumerator SendRequest(string uri, object bodyJson, string method, Action<DownloadHandler> onSuccess, Action<long> onError)
    {
        yield return SendRequest(uri, bodyJson, method, null, onSuccess, onError);
    }

    private void Update()
    {
        if (numOperations > 0 && !loadingPanel.activeSelf)
            loadingPanel.SetActive(true);
        else if (numOperations == 0 && loadingPanel.activeSelf)
            loadingPanel.SetActive(false);
    }

    private IEnumerator SendRequest(string uri, object bodyJson, string method, string token, Action<DownloadHandler> onSuccess, Action<long> onError)
    {
        using (var request = GetRequest(uri, method))
        {
            if (bodyJson != null)
            {
                var rawData = Encoding.UTF8.GetBytes(JsonUtility.ToJson(bodyJson));
                request.uploadHandler = new UploadHandlerRaw(rawData);
            }
            if (method.Equals(UnityWebRequest.kHttpVerbGET) || method.Equals(UnityWebRequest.kHttpVerbPOST))
            {
                request.downloadHandler = new DownloadHandlerBuffer();
                if (!string.IsNullOrEmpty(token))
                    request.SetRequestHeader("Authorization", token);
                request.SetRequestHeader("Content-Type", "application/json");
            }
            numOperations++;
            yield return request.SendWebRequest();
            numOperations = Mathf.Max(numOperations - 1, 0);
            if (request.result == UnityWebRequest.Result.ConnectionError || request.result == UnityWebRequest.Result.ProtocolError)
            {
                if (debugAuthResponse)
                    Debug.LogError($"{request.error} from {uri}");
                onError?.Invoke(request.result == UnityWebRequest.Result.ConnectionError ? -1 : request.responseCode);
            }
            else
            {
                if (debugAuthResponse)
                    Debug.Log($"<color=green>Authentication Successful!</color>\nFrom: {uri}\nResponse text: {request.downloadHandler.text}");
                onSuccess?.Invoke(request.downloadHandler);
            }
        }
    }

    private UnityWebRequest GetRequest(string uri, string method)
    {
        if (method.Equals(Texture))
            return UnityWebRequestTexture.GetTexture(uri);
        else
            return new UnityWebRequest(uri, method);
    }


}

public class RestApiObject
{
    public string uri;
    public object bodyJson;
    public string token;
    public Action<DownloadHandler> onSuccess;
    public Action<long> onError;
}
