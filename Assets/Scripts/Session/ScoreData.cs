using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreData
{
    public long InstanceId { get; private set; }
    public long LessonId { get; private set; }
    public long UserId { get; private set; }
    public string Name { get; private set; }
    public int Points { get; private set; }

    public ScoreData(JSONNode scoreData)
    {
        
        InstanceId = scoreData[nameof(InstanceId).ToCamelCase()];
        LessonId = scoreData[nameof(LessonId).ToCamelCase()];
        UserId = scoreData[nameof(UserId).ToCamelCase()];
        var userData = new UserData(scoreData["user"]);
        Name = $"{userData.firstname} {userData.lastname}";
        Points = scoreData[nameof(Points).ToCamelCase()];
    }

}
