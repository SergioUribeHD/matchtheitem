﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public static class User
{
    public static long Id { get; private set; }
    public static long OrgId { get; private set; }
    public static string Token { get; private set; }
    public static DateTime ExpirationDate { get; private set; }
    public static Texture2D Avatar
    {
        get => avatar;
        set
        {
            avatar = value;
            OnAvatarSet?.Invoke();
        }
    }
    public static string FirstName { get; private set; }
    public static string LastName { get; private set; }
    public static JSONNode CategoriesData { get; set; }
    public static JSONNode EnrollmentGroupsData { get; set; }
    public static List<Category> Categories { get; } = new List<Category>();
    public static Category CurrentCategory { get; set; }
    public static List<QuestionData> Questions { get; } = new List<QuestionData>();
    public static List<ScoreData> UsersScore { get; } = new List<ScoreData>();

    public static event Action OnAvatarSet = null;

    private static Texture2D avatar;

    public static void LogIn(UserData data)
    {
        Id = data.id;
        FirstName = data.firstname;
        LastName = data.lastname;
    }

    public static void LogOut()
    {
        Id = OrgId = 0;
        Token = FirstName = LastName = string.Empty;
        ExpirationDate = DateTime.MinValue;
        avatar = null;
        CategoriesData = EnrollmentGroupsData = null;
        Questions.Clear();
        
    }

    public static long GetEGId(long triviaId)
    {
        foreach (var item in EnrollmentGroupsData)
            if (item.Value["learning_object_id"] == triviaId) return item.Value["id"];
        return -1;
    }

    public static string GetEGName(long egId)
    {
        foreach (var item in EnrollmentGroupsData)
            if (item.Value["id"] == egId) return item.Value["name"];
        return null;
    }

}

[Serializable]
public class UserData
{
    public long id;
    public string firstname;
    public string lastname;

    //public UserData(JSONNode user)
    //{
    //    userId = user[nameof(userId)];
    //    var userObj = user[nameof(user)];
    //    firstname = userObj[nameof(firstname)].Value;
    //    lastname = userObj[nameof(lastname)].Value;
    //}

    public UserData(JSONNode user)
    {
        id = user[nameof(id)];
        firstname = user[nameof(firstname)].Value;
        lastname = user[nameof(lastname)].Value;
    }
}

[Serializable]
public class TeamsAndAttributesUser
{
    public long user_id;
    public long org_id;
    public Team[] listTeam;
    public Attr[] listAttr;

    //public TeamsAndAttributesUser(int userId, int orgId, IEnumerable<Team> teams, IEnumerable<Attr> attrs)
    //{
    //    user_id = userId;
    //    orgId =
    //}
}

[Serializable]
public class Team
{
    public long id;
    public long workteam;

    public static Team[] ToArray(string responseText)
    {
        var teams = SimpleJSON.JSON.Parse(responseText);
        var workTeams = new Team[teams.Count];
        for (int i = 0; i < workTeams.Length; i++)
            workTeams[i] = new Team { workteam = teams[i]["id"] };
        return workTeams;
    }
}

[Serializable]
public class Attr
{
    public long custom_attribute_id;
    public long custom_attribute_type;

    public Attr(long id, string attrStr)
    {
        custom_attribute_id = id;
        custom_attribute_type = long.TryParse(attrStr, out long result) ? result : 0;
    }

    public static Attr[] ToArray(string[] attrStrs)
    {
        var attrArray = new Attr[attrStrs.Length];
        for (int i = 0; i < attrArray.Length; i++)
            attrArray[i] = new Attr(i + 1, attrStrs[i]);
        return attrArray;
    }
}

[Serializable]
public class AttributesUser
{
    public string attr1;
    public string attr2;
    public string attr3;
    public string attr4;

    public string[] ToArray() => new string[4] { attr1, attr2, attr3, attr4 };
}

[Serializable]
public class EnrollmentGroupsUser
{
    public long[] listIds;

    public EnrollmentGroupsUser(JSONNode data)
    {
        var availableEnrollmentGroups = new List<EnrollmentGroup>();
        foreach (var enrollmentGroupBody in data)
        {
            int tsOpen = enrollmentGroupBody.Value["open_date"];
            int tsClose = enrollmentGroupBody.Value["close_date"];
            var enrollmentGroup = new EnrollmentGroup
            {
                id = enrollmentGroupBody.Value["learning_object_id"],
                openDate = tsOpen.ToDateTime(),
                closeDate = tsClose.ToDateTime()
            };
            //Debug.Log($"Id: {enrollmentGroup.id}\nOpening date:{enrollmentGroup.openDate}\nClosing date: {enrollmentGroup.closeDate}\nIs Enrolled: {enrollmentGroup.IsWithinDate}");
            //if (enrollmentGroup.IsWithinDate)
                availableEnrollmentGroups.Add(enrollmentGroup);
        }
        listIds = new long[availableEnrollmentGroups.Count];
        for (int i = 0; i < listIds.Length; i++)
            listIds[i] = availableEnrollmentGroups[i].id;
    }

}



[Serializable]
public class EnrollmentGroup
{
    public long id;
    public DateTime openDate;
    public DateTime closeDate;

    public bool IsWithinDate => DateTime.Now > openDate && DateTime.Now < closeDate;
}
