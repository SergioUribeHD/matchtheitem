using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasActivator : MonoBehaviour
{
    [SerializeField] private CanvasGroup canvasGroup = null;
    [SerializeField] private bool enableOnStart = false;

    private void OnValidate()
    {
        if (canvasGroup == null) return;
        SetCanvasActive(enableOnStart);
    }

    public void SetCanvasActive(bool active)
    {
        canvasGroup.alpha = active ? 1 : 0;
        canvasGroup.interactable = active;
        canvasGroup.blocksRaycasts = active;
    }
}
